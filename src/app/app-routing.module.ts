import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { DetailsComponent } from './pages/details/details.component';
import {AppGuard} from './guard/app.guard';
import {LoginComponent} from './pages/login/login.component';
import {SignupComponent} from './pages/signup/signup.component';
import {AuthGuard} from './guard/auth.guard';
import {AddComponent} from './pages/add/add.component';


const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [AppGuard]
  },
  {
    path: 'details/:city',
    component: DetailsComponent,
    canActivate: [AppGuard]
  },
  {
    path: 'add',
    component: AddComponent,
    canActivate: [AppGuard]
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'signup',
    component: SignupComponent,
    canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
