import {Component, OnDestroy, OnInit} from '@angular/core';
import {UiService} from './services/ui.service';
import {FbService} from './services/fb.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  constructor(
    public uiService: UiService,
    public fbService: FbService,
    public router: Router
  ) {

  }

  showMenu: boolean;
  darkModeActive: boolean;
  userEmail = '';
  loggedIn = this.fbService.isAuth();
  sub1;


  ngOnInit(): void {
    this.sub1 = this.uiService.darkModeState.subscribe((value) => {
      this.darkModeActive = value;
    });

    this.fbService.auth.userData().subscribe((user) => {
      this.userEmail = user.email;
    });

  }

  toggleMenu() {
    this.showMenu = !this.showMenu;
  }

  modeToggleSwitch() {
    this.uiService.darkModeState.next(!this.darkModeActive);
  }

  ngOnDestroy() {
    this.sub1.unsubscribe();
  }

  logOut() {
    this.toggleMenu();
    this.router.navigateByUrl('/login');
    this.fbService.auth.signout();
  }

}
