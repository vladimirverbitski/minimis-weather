import {Weather} from './weather';

export interface City {
  base: string;
  clouds: Array<[]>;
  cod: number;
  coord: {
    lat: number;
    lon: number;
  };
  dt: number;
  id: number;
  main: {
    grnd_level: number;
    humidity: number
    pressure: number;
    sea_level: number;
    temp: number;
    temp_max: number;
    temp_min: number;
  };
  name: string;
  sys: {
    country: string;
    sunrise: number;
    sunset: number;
  };
  timezone: number;
  weather: Array<Weather>;
  wind: {
    speed: number;
    deg: number;
  };
}
