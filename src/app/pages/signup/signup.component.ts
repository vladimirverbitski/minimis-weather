import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {FbService} from '../../services/fb.service';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  errorMessage = '';

  constructor(
    public router: Router,
    public fbService: FbService
  ) { }

  ngOnInit() {
  }

  signup(e) {
    this.fbService.signUp(e.target.email.value, e.target.password.value).pipe(first()).subscribe(() => {
      this.router.navigateByUrl('');
    }, (err) => {
      this.errorMessage = err;
      setTimeout(() => this.errorMessage = '', 2000);
    });
  }

}
