import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {FbService} from '../../services/fb.service';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  errorMessage = '';

  constructor(
    public router: Router,
    public fbService: FbService
  ) { }

  ngOnInit() {
  }

  login(e) {
    this.fbService.signIn(e.target.email.value, e.target.password.value).pipe(first()).subscribe(() => {
      this.router.navigateByUrl('');
    }, (err) => {
      this.errorMessage = err;
      setTimeout(() => this.errorMessage = '', 2000);
    });
  }

}
