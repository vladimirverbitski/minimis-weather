import { Component, OnInit } from '@angular/core';
import {FbService} from '../../services/fb.service';
import {Observable} from 'rxjs';
import {City} from '../../interfaces/city';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  cities: Observable<Array<City>>;

  constructor(
    public fbService: FbService
  ) { }

  ngOnInit() {
    this.cities = this.fbService.getCities();
  }

}
