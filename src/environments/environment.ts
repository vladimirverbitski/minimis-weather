// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  appID: 'ea259a9aefdda60cfe7b670c80d00d7e',
  config: {
    apiKey: 'AIzaSyB26hcbsNRgxto6TmhGCxB7FnMFLibtE78',
    authDomain: 'weather-app-c6d77.firebaseapp.com',
    databaseURL: 'https://weather-app-c6d77.firebaseio.com',
    projectId: 'weather-app-c6d77',
    storageBucket: 'weather-app-c6d77.appspot.com',
    messagingSenderId: '1074002871448'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
